<?php
class lastfm{
	private $username;
	private $error = false;
	private $apikey = '1908ef57457111c03c2f112a7f359f7d';
	
	public function __construct($username=''){
		$this->username = $username;
	}
	public function getRecentTracks(){
		if(empty($this->username)){
			$this->error = 'No username.';
			return false;
		}
		$results = $this->callapi('user.getrecenttracks');
		$results = $results['recenttracks']['track'];
		return $results;
	}
	
	public function getError(){
		return $error;
	}
	public function callapi($method='', $params=array()){
		// generate the url:
		$url = 'http://ws.audioscrobbler.com/2.0/?';
		$url .= 'method='.$method;
		$url .= '&user='.$this->username;
		$url .= '&api_key='.$this->apikey;
		foreach($params as $key=>$value){
			$url .= '&'.$key.'='.$value;
		}
		$url .= '&format=json';
		
		// call the api with this url:
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		// do it:
		$response = curl_exec($ch);
		curl_close ($ch);
		return json_decode($response, true);
	}
}