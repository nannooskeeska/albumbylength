<?php
class musixmatch{
	private $apikey = '4de58c60a2e3133d9dffe32821596bda';
	
	public function getLyrics($track_mbid){
		$results = $this->callapi('track.lyrics.get', array(
			'track_mbid'=>$track_mbid,
		));
		
		return $results;
	}
	
	public function callapi($method='', $params=array()){
		
		// generate the url:
		$url = 'http://api.musixmatch.com/ws/1.1/';
		$url .= $method;
		$url .= '?';
		foreach($params as $key=>$value){
			$url .= '&'.$key.'='.$value;
		}
		$url .= '&format=json';
		$url .= '&apikey='.$this->apikey;
		
		// call the api with this url:
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		// do it:
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}