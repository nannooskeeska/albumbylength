<?php
class musicbrainz{
	private $url = 'http://musicbrainz.org/';
	
	public function getTrackURL($track_mbid){
		return $this->url.'recording/'.$track_mbid;
	}
	public function getArtistURL($artist_mbid){
		return $this->url.'artist/'.$artist_mbid;
	}
	public function getArt($album_mbid){
		
		// generate the url:
		$url = 'http://coverartarchive.org/release/';
		$url .= $album_mbid;
		
		// call the api with this url:
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		
		// do it:
		$response = curl_exec($ch);
		
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($httpCode == 404){
			return '';
		}
		curl_close($ch);
		return $response;
	}
}