General
=======

Album by Length is our simple program where you can specify what genre of music you feel like listening to, and an approximate amount of time you have to listen, and it will suggest albums that fit your current need.


File Structure
==============

The "public_html" folder is where all public-facing files will go, which is called the "document root" the name of the folder is the apache default, so there should be no configuration necessary if we were to try to host this somewhere :)

All files above public_html are not public-facing but can still be included via PHP. This is a basic example of how this is typically accomplished.  You dont want to have PHP files that just contain classes public-facing because somebody could potentially game it to figure out and run code that you do not want them to run.


TODO
=====

- figure out exactly what features we want
- design the front end
- code the front end html/css/js
- integrate with the three web services
- hook up the front end design with the back end web service integration


Contributors
===========

- Andrew Petz (andrewpetz.com)
- Matt Larson (mistermashu.com)