<?php

$basepath = dirname($_SERVER['DOCUMENT_ROOT']).'/';

require_once $basepath.'includes/musixmatch.php';
$trackid = $_GET['trackid'];

if(empty($trackid)){
	die('no trackid given');
}

// get the lyrics:
$musixmatch = new musixmatch();
$lyrics = $musixmatch->getLyrics($trackid);
$body = json_decode($lyrics, true);
$body = $body['message']['body']['lyrics']['lyrics_body'];

echo nl2br($body);