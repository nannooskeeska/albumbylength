<?php

$basepath = dirname($_SERVER['DOCUMENT_ROOT']).'/';

require_once $basepath.'includes/musicbrainz.php';
$musicbrainz = new musicbrainz();
$album_mbid = $_GET['album_mbid'];

if(empty($album_mbid)){
	die();
}

$art = $musicbrainz->getArt($album_mbid);

echo $art;