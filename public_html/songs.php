<?php

//$doc_root = $_SERVER['DOCUMENT_ROOT'];

$basepath = dirname($_SERVER['DOCUMENT_ROOT']).'/';

// require all the files you need:
require_once $basepath.'includes/lastfm.php';
require_once $basepath.'includes/musicbrainz.php';

// get the submitted username and redirect them back to the index page if we don't know their username:
$username = $_GET['txtLastFmUsername'];
if(empty($username)){
	header('Location: /');
	exit;
}

// get songs for the given username:
require_once $basepath.'includes/lastfm.php';
$lastfm = new lastfm($username);
$tracks = $lastfm->getRecentTracks();
if($tracks === false){
	$error = $lastfm->getError();
}

include $basepath.'includes/header.php';

// make our musicbrainz object for getting musicbrainz URLs:
$musicbrainz = new musicbrainz();

?>

<? if(empty($error)){?>
<div id="container">
  <table class="tg">
    <tr>
      <th></th>
      <th>Song Name</th>
      <th>Artist</th>
      <th>Lyrics</th>
  </tr>
  <? foreach($tracks as $track){?>
  <tr>
     <td><img class="mb-album-art" data-mbid="<?=$track['album']['mbid']?>" src="" alt=""/></td>
     <td><a href="<?=$musicbrainz->getTrackURL($track['mbid'])?>"><?=$track['name']?></a></td>
     <td><a href="<?=$musicbrainz->getArtistURL($track['artist']['mbid'])?>"><?=$track['artist']['#text']?></a></td>
     <td><? if(!empty($track['mbid'])){?><a class="button" onclick="loadlyrics('<?=$track['mbid']?>');">get</a><? }?></td>
 </tr>
 <? }?>
</table>

<div id="lyrics"></div>
</div>

<? }else{ // no output because there was an api error:?>
<div class="error"><?=$error?></div>
<? }?>

<? include $basepath.'includes/footer.php'?>