CS 268 Web Services Assignment
==============================

Andrew Petz and Matt Larson
---------------------------

We did not seem to have too many issues with this assignment. I think that the biggest trouble we had was just figuring out some of the API calls and parsing through their results. There was also a period of time where we couldn't figure out how to get the album art to display at the correct resolution. I mostly did the frontend, and that was no trouble at all.